import random
import time

words = ['tetra', 'gidra', 'canna', 'binol', 'gomodril',
         'vitalya', 'pisos', 'nu', 'ti', 'chmonya']

storage_dict = {'key': 'value'}


def link_generator():
    new_link = (
        f'short://{random.choice(words)}-'
        f'{random.choice(words)}-'
        f'{random.choice(words)}'
    )
    return new_link


def dictionary(storage: dict):
    text = input('Insert the text:')
    if 'short://' in text:
        if text in storage:
            print(storage[text])
        else:
            print('No such link already has been saved!')
    else:
        storage[link_generator()] = text


while True:
    dictionary(storage_dict)
    print(storage_dict)
